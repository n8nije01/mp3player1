import { Component } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  private totalSpace: number;
  private usedSpace: number;
  private nbrOfTracks: number;
  private remainingSpace: number;
  private averageSpace: number;
  private nbrOfTracksLeft: number;


  constructor() {}

  calculate() {
    this.remainingSpace = this.totalSpace - this.usedSpace;
    this.averageSpace = this.usedSpace / this.nbrOfTracks;
    this.nbrOfTracksLeft = this.remainingSpace / this.averageSpace;
    this.nbrOfTracksLeft = Math.floor(this.nbrOfTracksLeft);
  }




}
